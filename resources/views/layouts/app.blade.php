<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ config('app.name', 'TTCL') }}</title>

    <link rel="icon" href="{{ asset('img/favicon.png') }}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.5.3/css/bulma.min.css">

    <style>
        .container {
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>

</head>
<body>
    <div id="app">

    <section class="hero is-dark">
      &nbsp;
    </section>

        @include('partials.navbar')

        <br>

        @if (session('status'))
            <div class="notification is-primary has-text-centered">
                {{ session('status') }}
            </div>            
        @endif        

        @yield('content')

        <br>
        <footer class="footer">
          <div class="container">
            <div class="content has-text-centered">
              <p>&copy; {{ date('Y') }} All Rights Reserved</p>
            </div>
          </div>
        </footer>        

    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>

@extends('layouts.app')

@section('title', 'Verified Students')

@section('content')

    @include('partials.hero')

    <div class="container">
        
        @include('partials.tabs')

		@if($students->count())
        	@include('students.table')
        @else
        	<p class="has-text-centered">No student</p>
        @endif

    </div>
@endsection

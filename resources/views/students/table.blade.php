<table class="table is-bordered is-striped is-narrow is-fullwidth">
	<thead>
		<tr>
			<th>SN</th>
			<th>First name</th>
			<th>Last name</th>
			<th>Phone number</th>
			<th>Email</th>
			<th>Institution</th>
			<th>Registered</th>
			@if(Request::is('dashboard/students/verified'))
				<th>Verified</th>
			@endif
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@php $i = 1 @endphp
		@foreach($students as $student)
				<tr>
					<td>{{ $i++ }}.</td>
					<td>{{ $student->first_name }}</td>
					<td>{{ $student->last_name }}</td>
					<td>{{ $student->phone_number }}</td>
					<td>{{ $student->email }}</td>
					<td>{{ $student->institution }}</td>
					<td>{{ $student->created_at->diffForHumans() }}</td>
					@if(Request::is('dashboard/students/verified'))
						<td>{{ $student->verified_at->diffForHumans() }}</td>
					@endif					
					<td>
						<a href="{{ route('students.show', $student->id) }}">
							View
						</a>
					</td>
				</tr>
		@endforeach
	</tbody>
</table>


{{ $students->links() }}

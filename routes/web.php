<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::group(['middleware' => 'auth'], function () {

	Route::get('/dashboard', 'HomeController@index')->name('home');

	Route::get('dashboard/students/verified', 'StudentController@verified')->name('students.verified');
	Route::get('dashboard/students/pending', 'StudentController@unverified')->name('students.unverified');

	Route::resource('dashboard/students', 'StudentController')->only(['index', 'show']);

	Route::post('dashboard/students/{student}/verify', 'StudentController@verify')->name('verify');
	Route::post('dashboard/students/{student}/unverify', 'StudentController@unverify')->name('unverify');

});


Route::get('free-registration', 'StudentController@create')->name('registration.create');
Route::post('free-registration', 'StudentController@store')->name('registration.store');

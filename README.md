# University Promotion Registration System

This system allows university students to be registered for university promotion. 

## Requirements

The student will be required to provide the following information:-

- First name

- Last name

- Phone number

- Email address

- Name of Education Institution that he/she is in

- Image of Identification card


## Verification

After submitting the above information, he/she will wait for confirmation. An authorized personel will review the submitted information and then determines whether that student is valid or not for university promotion. Then the student will be notified whether has been accepted or rejected for promotion.


## Demo

[https://uni-promo-reg.herokuapp.com](https://uni-promo-reg.herokuapp.com)

**Credentials**

To verify the applications click the following link

[https://uni-promo-reg.herokuapp.com/login](https://uni-promo-reg.herokuapp.com/login)

And login using the following information

- Email: `demo@example.com`

- Password: `demo`


## Isssues and Suggestions

Please submit any issues, comments or suggestions through the following link

[https://bitbucket.org/josephmtinangi/ttcl-university-promo-registration/issues](https://bitbucket.org/josephmtinangi/ttcl-university-promo-registration/issues)
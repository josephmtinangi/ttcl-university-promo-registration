@extends('layouts.app')

@section('title', 'Free Registration')

@section('content')

<section class="hero is-transparent is-bold">
  <div class="hero-body">
    <div class="container">
      <h1 class="title has-text-centered">
        Register For Free
      </h1>
    </div>
  </div>
</section>

<div class="container">
    
<div class="columns">
  <div class="column is-half is-offset-one-quarter">

    @include('errors.list')

    <form action="{{ route('registration.store') }}" method="POST" enctype="multipart/form-data">
        
        {{ csrf_field() }}
    
        @include('students._form')

    </form>


  </div>
</div>

</div>

@endsection

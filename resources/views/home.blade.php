@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')

    @include('partials.hero')

    <div class="container">
        
        @include('partials.tabs')

        <h1 class="title has-text-centered">{{ $studentsCount }} Total</h1>

    </div>
@endsection

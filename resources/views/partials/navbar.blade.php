<nav class="navbar is-light">
  <div class="container">
  <div class="navbar-brand">
    <a class="navbar-item" href="{{ config('app.url') }}">
      {{ config('app.name') }}
    </a>
    <div class="navbar-burger burger" data-target="navMenuTransparentExample">
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>

  <div id="navMenuTransparentExample" class="navbar-menu">

    <div class="navbar-end">

      @if(Auth::user())
        <a class="navbar-item" href="{{ route('home') }}">Dashboard</a>
        <a class="navbar-item" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
            Logout
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
      @endif

      <div class="navbar-item">
        <div class="field is-grouped">
          <p class="control">
            <a class="button is-primary" href="{{ route('registration.create') }}">
              <span>Register for Free</span>
            </a>
          </p>
        </div>
      </div>



    </div>
  </div>    
  </div>
</nav>
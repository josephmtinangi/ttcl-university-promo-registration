
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>{{ config('app.name') }}</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <style>
    	body {
    		margin: 0;
    		padding: 0;
		    font-family:Trebuchet MS,sans-serif;
			background:url('/img/bg.jpg') repeat-y center top;
			background-size: cover;
    		padding-top: 70px;
    	}
    	h2 {
    		font-size: 8rem;
    	}  
    	.carousel-caption p {
    		font-size: 2rem;
    		color: #FDCE07;
    		font-weight: 900;
    	}  	
    	.fa-icon {
    		color: #FDCE07;
    		font-size: 10rem;
    	}
    	.btn {
    		background-color: #FDCE07;
    		border-color: #FDCE07;
    		border-radius: 0;
    	}
    	footer {
    		margin-top: 10rem;
    		padding-top: 5rem;
    		padding-bottom: 3rem;
    		background-color: #2F2F2F;
    		color: #FFF;
    	}

    	/*Navbar*/

		#custom-bootstrap-menu.navbar-default .navbar-brand {
		    color: rgba(0, 0, 0, 1);
		    font-weight: 500;
		}
		#custom-bootstrap-menu.navbar-default {
		    font-size: 2rem;
		    background-color: rgba(253, 206, 7, 1);
		    border-bottom-width: 0px;
		    font-weight: 500;
		    padding: 4px;
		}
		#custom-bootstrap-menu.navbar-default .navbar-nav>li>a {
		    color: rgba(0, 0, 0, 1);
		    background-color: rgba(248, 248, 248, 0);
		}
		#custom-bootstrap-menu.navbar-default .navbar-nav>li>a:hover,
		#custom-bootstrap-menu.navbar-default .navbar-nav>li>a:focus {
		    color: rgba(255, 255, 255, 1);
		    background-color: rgba(0, 151, 209, 1);
		}
		#custom-bootstrap-menu.navbar-default .navbar-nav>.active>a,
		#custom-bootstrap-menu.navbar-default .navbar-nav>.active>a:hover,
		#custom-bootstrap-menu.navbar-default .navbar-nav>.active>a:focus {
		    color: rgba(255, 255, 255, 1);
		    background-color: rgba(0, 151, 209, 1);
		}
		#custom-bootstrap-menu.navbar-default .navbar-toggle {
		    border-color: #0097d1;
		}
		#custom-bootstrap-menu.navbar-default .navbar-toggle:hover,
		#custom-bootstrap-menu.navbar-default .navbar-toggle:focus {
		    background-color: #0097d1;
		}
		#custom-bootstrap-menu.navbar-default .navbar-toggle .icon-bar {
		    background-color: #0097d1;
		}
		#custom-bootstrap-menu.navbar-default .navbar-toggle:hover .icon-bar,
		#custom-bootstrap-menu.navbar-default .navbar-toggle:focus .icon-bar {
		    background-color: #fdce07;
		}


    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
	
	<div id="app">


	<div class="container">


		<div id="custom-bootstrap-menu" class="navbar navbar-default navbar-static-top" role="navigation">
		    <div class="container-fluid">

		        <div class="navbar-header">
		        	<a href="{{ config('app.url') }}" class="navbar-left"><img src="/img/logo.JPG"></a>
		        </div>

				<ul class="nav navbar-nav">
					<li>
						<a href="#">{{ config('app.name') }}</a>
					</li>
				</ul>

				@if(Auth::check())
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="{{ route('home') }}">Dashboard</a>
						</li>
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>						
					</ul>
				@endif

		    </div>
		</div>

		<div id="carousel-id" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#carousel-id" data-slide-to="0" class=""></li>
				<li data-target="#carousel-id" data-slide-to="1" class=""></li>
				<li data-target="#carousel-id" data-slide-to="2" class="active"></li>
			</ol>
			<div class="carousel-inner">
				<div class="item">
					<img src="/img/pic1-min.jpeg" alt="">
					<div class="container">
						<div class="carousel-caption">
							<p>Affordable Prices</p>
						</div>
					</div>
				</div>
				<div class="item">
					<img src="/img/pic2-min.jpeg" alt="">
					<div class="container">
						<div class="carousel-caption">
							<p>High Voice Clarity</p>
						</div>
					</div>
				</div>
				<div class="item active">
					<img src="/img/pic3-min.jpg" alt="">
					<div class="container">
						<div class="carousel-caption">
							<p>High Speed Internet</p>
						</div>
					</div>
				</div>
			</div>
			<a class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
			<a class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
		</div>

		<h2 class="text-center">Benefits</h2>
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<div class="thumbnail text-center">
					<i class="fa fa-wifi fa-5x fa-icon"></i>
					<div class="caption">
						<h3>High Speed Internet</h3>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<div class="thumbnail text-center">
					<i class="fa fa-microphone fa-5x fa-icon"></i>
					<div class="caption">
						<h3>High Voice Clarity</h3>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<div class="thumbnail text-center">
					<i class="fa fa-money fa-5x fa-icon"></i>
					<div class="caption">
						<h3>Affordable Prices</h3>
					</div>
				</div>
			</div>
		</div>

		@if (session('status'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p class="lead">
					<strong>Success!</strong> {{ session('status') }}
				</p>
			</div>
		@endif

		<h2 class="text-center">Register</h2>
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">

				<form action="{{ route('registration.store') }}" method="POST" enctype="multipart/form-data" role="form">
					{{ csrf_field() }}

					<legend>Please fill your information bellow</legend>
				
					<div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
						<label for="first_name">First name *</label>
						<input type="text" class="form-control input-lg" name="first_name" id="first_name" value="{{ old('first_name') }}" placeholder="First name" required>
                        @if ($errors->has('first_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif						
						<p class="help-block">
							As in your ID card
						</p>
					</div>

					<div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
						<label for="last_name">Last name *</label>
						<input type="text" class="form-control input-lg" name="last_name" id="last_name" value="{{ old('last_name') }}" placeholder="Last name" required>
						@if ($errors->has('last_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif
						<p class="help-block">
							As in your ID card
						</p>
					</div>

					<div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
						<label for="phone_number">Phone number *</label>
						<input type="tel" class="form-control input-lg" name="phone_number" id="phone_number" value="{{ old('phone_number') }}" placeholder="Phone number" required>
						@if ($errors->has('phone_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone_number') }}</strong>
                            </span>
                        @endif
						<p class="help-block">
							Enter in this format 073
						</p>
					</div>

					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="email">Email Address *</label>
						<input type="email" class="form-control input-lg" name="email" id="email" value="{{ old('email') }}" placeholder="Email Address" required>
						@if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
						<p class="help-block">
							You will have to verify this email address
						</p>
					</div>

					<div class="form-group{{ $errors->has('institution') ? ' has-error' : '' }}">
						<label for="institution">Education institution *</label>
						<input type="text" class="form-control input-lg" name="institution" id="institution" value="{{ old('institution') }}" placeholder="Education institution" required>
						@if ($errors->has('institution'))
                            <span class="help-block">
                                <strong>{{ $errors->first('institution') }}</strong>
                            </span>
                        @endif
						<p class="help-block">
							Write the name of education instution as written in your ID card
						</p>
					</div>
				
					<div class="form-group{{ $errors->has('id_card') ? ' has-error' : '' }}">
						<label for="id_card">ID card *</label>
						<input type="file" name="id_card" id="id_card" required>
                        @if ($errors->has('id_card'))
                            <span class="help-block">
                                <strong>{{ $errors->first('id_card') }}</strong>
                            </span>
                        @endif						
						<p class="help-block">
							Please select an image of your ID card
						</p>
					</div>

					<p>
						<strong>By clicking submit you agree to the Terms &amp; Conditions</strong>
					</p>
				
					<button type="submit" class="btn btn-primary btn-block btn-lg">Submit</button>
				</form>
			</div>
		</div>

	</div>

	<footer class="text-center">
		<p>
			&copy; {{ date('Y') }} {{ config('app.name') }} All rights reserved
		</p>
		<p>
			<small>
				<a href="{{ route('login') }}">Staff Login</a>
			</small>
		</p>
		<p>
			Terms &amp; Conditions Apply.
		</p>
	</footer>

	</div>

    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/vue.js"></script>
    <script>
    	new Vue({
    		el: '#app',
    		data: {

    		},
    		mounted() {
    			console.log('Ready');
    		}
    	})
    </script>
  </body>
</html>

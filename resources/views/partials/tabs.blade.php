<div class="tabs is-centered is-medium">
  <ul>
    <li class="{{ Request::is('dashboard') ? 'is-active' : '' }}"><a href="{{ route('home') }}">Dashboard</a></li>
    <li class="{{ Request::is('dashboard/students') ? 'is-active' : '' }}"><a href="{{ route('students.index') }}">All</a></li>
    <li class="{{ Request::is('dashboard/students/verified') ? 'is-active' : '' }}"><a href="{{ route('students.verified') }}">Verified</a></li>
    <li class="{{ Request::is('dashboard/students/pending') ? 'is-active' : '' }}"><a href="{{ route('students.unverified') }}">Pending</a></li>
  </ul>
</div>
<div class="field">
  <label class="label">First name</label>
  <div class="control">
    <input class="input" type="text" name="first_name" placeholder="First name">
  </div>
</div>

<div class="field">
  <label class="label">Last name</label>
  <div class="control">
    <input class="input" type="text" name="last_name" placeholder="Last name">
  </div>
</div>

<div class="field">
  <label class="label">Phone number</label>
  <div class="control">
    <input class="input" type="tel" name="phone_number" placeholder="Phone number">
  </div>
</div>

<div class="field">
  <label class="label">Email</label>
  <div class="control">
    <input class="input" type="email" name="email" placeholder="Email">
  </div>
</div>

<div class="field">
  <label class="label">Education Institution</label>
  <div class="control">
    <input class="input" type="text" name="institution" placeholder="Education Institution">
  </div>
</div>

<div class="field">
  <label class="label">Student ID card</label>
<div class="file">
  <label class="file-label">
    <input class="file-input" type="file" name="id_card">
    <span class="file-cta">
      <span class="file-icon">
        <i class="fa fa-upload"></i>
      </span>
      <span class="file-label">
        Choose the ID card
      </span>
    </span>
  </label>
</div>  
</div>


<div class="field">
  <div class="control">
    <button class="button is-primary">Register</button>
  </div>
</div>
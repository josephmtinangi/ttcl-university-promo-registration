@extends('layouts.app')

@section('title', $student->first_name . ' ' . $student->last_name)

@section('content')

    @include('partials.hero')

    <div class="container">
        
        @include('partials.tabs')

		       
		<div class="columns">
		  <div class="column">

			<table class="table">
				<tbody>
					<tr>
						<th>First name</th>
						<td>{{ $student->first_name }}</td>
					</tr>
					<tr>
						<th>Last name</th>
						<td>{{ $student->last_name }}</td>
					</tr>
					<tr>
						<th>Phone number</th>
						<td>{{ $student->phone_number }}</td>
					</tr>
					<tr>
						<th>Email</th>
						<td>{{ $student->email }}</td>
					</tr>
					<tr>
						<th>Institution</th>
						<td>{{ $student->institution }}</td>
					</tr>
					<tr>
						<th>Registration date</th>
						<td>{{ $student->created_at }}</td>
					</tr>
					<tr>
						<th>Status</th>
						<td>
							@isset($student->verified_at)
								Verified
							@endisset
							@empty($student->verified_at)
								Unverified
							@endempty
						</td>
					</tr>
					@isset($student->verified_at)
						<tr>
							<th>Date of verification</th>
							<td>{{ $student->verified_at }}</td>
						</tr>
					@endisset
				</tbody>
			</table>
		    
		    @empty($student->verified_at)
			    <form action="{{ route('verify', $student->id) }}" method="POST">
			    	{{ csrf_field() }}

			    	<button type="submit" class="button is-primary">Verify</button>
			    </form>
		    @endempty
		    @isset($student->verified_at)
			    <form action="{{ route('unverify', $student->id) }}" method="POST">
			    	{{ csrf_field() }}

			    	<button type="submit" class="button is-primary">Revoke</button>
			    </form>
		    @endisset

		  </div>

		 <div class="column">
		 	<h2>ID Card</h2>

			<img src="{{ asset('storage/' . $student->id_card_path) }}" alt="">

		 </div>

		</div>


    </div>
@endsection

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
    	'first_name',
    	'last_name',
    	'phone_number',
    	'email',
    	'institution',
    	'id_card_path',
    ];

    protected $dates = ['verified_at'];
}

@extends('layouts.app')

@section('title', 'Login')

@section('content')

<section class="hero is-transparent">
  <div class="hero-body">
    <div class="container">
      <h1 class="title has-text-centered">
        Login
      </h1>
    </div>
  </div>
</section>

<div class="container">
    <div class="columns">
        <div class="column is-half is-offset-one-quarter">

            @include('errors.list')
            
            <form action="{{ route('login') }}" method="POST">
                {{ csrf_field() }}

                <div class="field">
                  <label class="label">Email</label>
                  <div class="control">
                    <input class="input" type="email" name="email" placeholder="Email">
                  </div>
                </div>

                <div class="field">
                  <label class="label">Password</label>
                  <div class="control">
                    <input class="input" type="password" name="password" placeholder="Password">
                  </div>
                </div>

                <div class="field">
                  <div class="control">
                    <button type="submit" class="button is-primary">Login</button>
                  </div>
                </div>                

            </form>
        </div>
    </div>
</div>
@endsection

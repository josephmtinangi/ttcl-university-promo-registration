<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Student::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone_number' => $faker->phoneNumber,
        'email' => $faker->freeEmail,
        'institution' => $faker->company,
        'id_card_path' => 'http://via.placeholder.com/640x300',
    ];
});

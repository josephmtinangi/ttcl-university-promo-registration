<section class="hero is-transparent">
  <div class="hero-body">
    <div class="container">
      <h1 class="title has-text-centered">
        @yield('title')
      </h1>
    </div>
  </div>
</section>
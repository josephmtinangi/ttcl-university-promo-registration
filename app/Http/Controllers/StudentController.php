<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StudentController extends Controller
{

    public function index()
    {
        $students = Student::latest()->paginate(20);
        return view('students.index', compact('students'));
    }

    public function verified()
    {
        $students = Student::whereNotNull('verified_at')->orderBy('verified_at', 'DESC')->paginate(20);
        return view('students.verified', compact('students'));
    }

    public function unverified()
    {
        $students = Student::whereNull('verified_at')->latest()->paginate(20);
    	return view('students.unverified', compact('students'));
    }

    public function create()
    {
        return view('students.create');
    }

    public function store(Request $request)
    {
    	$data = $this->validate($request, [
    		'first_name' => 'required|string',
    		'last_name' => 'required|string',
    		'phone_number' => 'required|unique:students',
    		'email' => 'required|string|email|max:255|unique:students',
    		'institution' => 'required|string',
    		'id_card' => 'required',
    	]);

    	$path = $request->id_card->store('student-id-cards');

    	Student::create([
    		'first_name' => $data['first_name'],
    		'last_name' => $data['last_name'],
    		'phone_number' => $data['phone_number'],
    		'email' => $data['email'],
    		'institution' => $data['institution'],
    		'id_card_path' => $path,
    	]);

        $request->session()->flash('status', 'Registration successful! Now wait you will be notified soon.');

    	return back();
    }

    public function show(Student $student)
    {
        return view('students.show', compact('student'));
    }

    public function verify(Request $request, Student $student)
    {
        $student->verified_at = Carbon::now();
        $student->verified_by = auth()->id();
        $student->save();

        $request->session()->flash('status', 'Verified successful!');

        return back();
    }

    public function unverify(Request $request, Student $student)
    {
        $student->verified_at = null;
        $student->verified_by = null;
        $student->save();

        $request->session()->flash('status', 'Revoked successful!');

        return back();
    }
}
